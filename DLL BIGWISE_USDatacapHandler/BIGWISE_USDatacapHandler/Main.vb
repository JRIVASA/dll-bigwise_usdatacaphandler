﻿Imports DSIEMVXLib
Imports DSIPDCXLib
Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Linq
Imports System.Xml.XPath
Public Class Main

    Private DCI As Object, DRI As Object

    Private Datacap_Request_Info As Object
    Public Property RequestInfo() As Object
        Get
            Return Datacap_Request_Info
        End Get
        Set(ByVal value As Object)
            Datacap_Request_Info = value
        End Set
    End Property

    Private Datacap_Response_Info As Object
    Public Property ResponseInfo() As Object
        Get
            Return Datacap_Response_Info
        End Get
        Set(ByVal value As Object)
            Datacap_Response_Info = value
        End Set
    End Property

    Private VB6Aux As Object
    Public Property VB6AuxObj() As Object
        Get
            Return VB6Aux
        End Get
        Set(ByVal value As Object)
            VB6Aux = value
        End Set
    End Property

    Private EMVHandler As DsiEMVX
    Private PDCHandler As DsiPDCX

    Private LocalData As Dictionary(Of Object, Object)

    Public Sub BeginProcess()

        If LocalData Is Nothing Then LocalData = New Dictionary(Of Object, Object) ' Uso Interno de Esta DLL .NET - Exclusivo

        If EMVHandler Is Nothing Then EMVHandler = New DsiEMVX
        If PDCHandler Is Nothing Then PDCHandler = New DsiPDCX

        'Dim ResponseInfo As Microsoft.VisualBasic.Collection
        ResponseInfo = New Microsoft.VisualBasic.Collection

        Dim EMVDevices = Nothing
        Dim PDCDevices = Nothing

        'ResponseInfo.Add(EMVHandler.GetDevicesInfo(), "ChipDevicesInfo")
        'ResponseInfo.Add(PDCHandler.GetDevicesInfo, "SwipeDevicesInfo")

        'MsgBox("Processing Request...")
        'MsgBox("EMV Ready Devices: \n\n" & ResponseInfo("ChipDevicesInfo"))
        'MsgBox("Regular Ready Devices: \n\n" & ResponseInfo("SwipeDevicesInfo"))

        ' To do ...

        DCI = Datacap_Request_Info

        If DCI.Exists("TrainingMode") Then
            If DCI("TrainingMode") Then
                EMVDevices = EMVHandler.GetDevicesInfo
                PDCDevices = PDCHandler.GetDevicesInfo

                Dim FormattedEMVDevices = GetFormattedXml(EMVDevices)
                Dim FormattedPDCDevices = GetFormattedXml(PDCDevices)

                Console.WriteLine(EMVDevices)
                Console.WriteLine(PDCDevices)
            End If
        End If

        If DCI("EMV_PendingParamDownload") Then

            EMVParamDownload()

            If DRI("OperationSuccess") Then
                DRI("EMV_PendingParamDownload") = False
            Else
                DRI("EMV_PendingParamDownload") = True
            End If

            If DCI("MainOperationType") = "EMVParamDownload" Then ' Avoid doing it twice.
                Exit Sub
            End If

        End If

        Select Case DCI("MainOperationType")

            Case "Sale", "CheckBalance", "EBTFoodStampBalance", "ManualEBTFoodStampBalance", "EBTCashBalance", "ManualEBTCashBalance"

                If DCI("EMVEnabled") Then
                    EMVSale()
                Else
                    PDCSale()
                End If

            Case "RelatedTransVoid", "Refund", "Return", "ManualReturn", _
            "EBTFoodStampReturn", "ManualEBTFoodStampReturn", _
            "EBTCashReturn", "ManualEBTCashReturn", "ManualEBTVoucherReturn"

                If DCI("EMVEnabled") Then
                    EMVReturn()
                Else
                    PDCReturn()
                End If

            Case "EMVParamDownload"

                EMVParamDownload()

                If DRI("OperationSuccess") Then
                    DRI("EMV_PendingParamDownload") = False
                Else
                    DRI("EMV_PendingParamDownload") = True
                End If

            Case "GetSignature"

                GetSignature()

            Case "BatchSummary"

                EMVBatchSummary()

            Case "BatchClose"

                EMVBatchClose()

            Case "PDCBatchSummary" ' Just in case both are needed.

                PDCBatchSummary()

            Case "PDCBatchClose" ' Just in case both are needed.

                PDCBatchClose()

        End Select

    End Sub
    Function CheckResponse(RespXML As String, ForWhat() As String) As Boolean
        LocalData("ValidResponse") = False
        If Not String.IsNullOrEmpty(RespXML) Then
            Try
                LocalData("ResponseDoc") = XDocument.Parse(RespXML)
                LocalData("RStream") = LocalData("ResponseDoc").Element("RStream")
                LocalData("CmdResponse") = LocalData("RStream").Element("CmdResponse")
                LocalData("CmdStatus") = LocalData("CmdResponse").Element("CmdStatus")
                LocalData("ValidResponse") = True
                For Each S As String In ForWhat
                    If (String.Equals(LocalData("CmdStatus").Value, S, StringComparison.OrdinalIgnoreCase)) Then
                        Return True
                    End If
                Next
            Catch Any As Exception

            End Try
        End If
        Return False
    End Function

    Function CheckElement(XPath As String, ForWhat() As String) As Boolean
        Try
            Dim TmpDoc As XDocument = LocalData("ResponseDoc")
            LocalData("TmpElement") = TmpDoc.XPathSelectElement(XPath)
            If LocalData("TmpElement") Is Nothing Then Return False
            LocalData("TmpElementValue") = LocalData("TmpElement").Value
            For Each S As String In ForWhat

                If (String.Equals(LocalData("TmpElementValue"), S, StringComparison.OrdinalIgnoreCase)) Then
                    Return True
                End If

            Next
        Catch Any As Exception

        End Try
        Return False
    End Function

    Function GetElement(XPath As String) As Boolean
        Try
            Dim TmpDoc As XDocument = LocalData("ResponseDoc")
            'Dim NSMgr = New XmlNamespaceManager(New NameTable())
            'NSMgr.AddNamespace("None", "")
            'LocalData("TmpElementValue") = TmpDoc.XPathSelectElement(XPath, NSMgr)
            LocalData("TmpElement") = TmpDoc.XPathSelectElement(XPath)
            If LocalData("TmpElement") Is Nothing Then Return False
            Return True
        Catch Any As Exception

        End Try
        Return False
    End Function

    Function GetElementValue(XPath As String) As Boolean
        Try
            Dim TmpDoc As XDocument = LocalData("ResponseDoc")
            'Dim NSMgr = New XmlNamespaceManager(New NameTable())
            'NSMgr.AddNamespace("None", "")
            'LocalData("TmpElementValue") = TmpDoc.XPathSelectElement(XPath, NSMgr).Value
            LocalData("TmpElementValue") = TmpDoc.XPathSelectElement(XPath).Value
            Return True
        Catch Any As Exception

        End Try
        Return False
    End Function

    Private Sub EMVParamDownload(Optional ByVal PostProcess As Boolean = False)

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Admin>"

        TXML += "<HostOrIP>" + DCI("EMV_HostOrIp") + "</HostOrIP>"
        TXML += "<IpPort>" + DCI("EMV_IpPort").ToString + "</IpPort>"
        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("EMV_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("EMV_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        If Not String.IsNullOrEmpty(DCI("EMV_UserTrace")) Then
            TXML += "<UserTrace>" + DCI("EMV_UserTrace") + "</UserTrace>"
        End If

        TXML += "<TranCode>" + "EMVParamDownload" + "</TranCode>"
        TXML += "<SecureDevice>" + DCI("EMV_SecureDevice") + "</SecureDevice>"
        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"
        TXML += "<SequenceNo>" + DCI("EMV_SequenceNo") + "</SequenceNo>"

        TXML += "</Admin>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
              "<RStream>" +
                  "<CmdResponse>" +
                      "<ResponseOrigin>Processor</ResponseOrigin>" +
                      "<DSIXReturnCode>000000</DSIXReturnCode>" +
                      "<CmdStatus>Success</CmdStatus>" +
                      "<TextResponse>SUCCESS</TextResponse>" +
                      "<SequenceNo>" + (CDbl(DCI("EMV_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                      "<UserTrace>Dev1</UserTrace>" +
                  "</CmdResponse>" +
                  "<TranResponse>" +
                      "<TranCode>EMVParamDownload</TranCode>" +
                  "</TranResponse>" +
              "</RStream>"
        Else
            RespXML = EMVHandler.ProcessTransaction(TXML)
        End If

        Dim FormattedRespXML = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        If PostProcess Then

            DRI("PostProcess_EMVParamDownload_RequestXML") = TXML
            DRI("PostProcess_EMVParamDownload_Completed") = CheckResponse(RespXML, {"Success"})
            DRI("PostProcess_EMVParamDownload_ResponseXML") = RespXML
            DRI("PostProcess_EMVParamDownload_FormattedResponseXML") = FormattedRespXML

        Else


            DRI("OperationRequestXML") = TXML
            DRI("OperationSuccess") = CheckResponse(RespXML, {"Success"})
            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXML

        End If

        DRI("EMV_SequenceNo") = "0010010010"
        DRI("PDC_SequenceNo") = DCI("PDC_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("EMV_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("EMV_SequenceNo") = DRI("EMV_SequenceNo")

    End Sub

    Private Sub EMVPadReset()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Transaction>"

        TXML += "<HostOrIP>" + DCI("EMV_HostOrIp") + "</HostOrIP>"
        TXML += "<IpPort>" + DCI("EMV_IpPort").ToString + "</IpPort>"
        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("EMV_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("EMV_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        If Not String.IsNullOrEmpty(DCI("EMV_UserTrace")) Then
            TXML += "<UserTrace>" + DCI("EMV_UserTrace") + "</UserTrace>"
        End If

        TXML += "<TranCode>" + "EMVPadReset" + "</TranCode>"
        TXML += "<SecureDevice>" + DCI("EMV_SecureDevice") + "</SecureDevice>"
        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"
        TXML += "<SequenceNo>" + DCI("EMV_SequenceNo") + "</SequenceNo>"

        TXML += "</Transaction>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
              "<RStream>" +
                  "<CmdResponse>" +
                      "<ResponseOrigin>Client</ResponseOrigin>" +
                      "<DSIXReturnCode>000000</DSIXReturnCode>" +
                      "<CmdStatus>Success</CmdStatus>" +
                      "<TextResponse>Reset Successful.</TextResponse>" +
                      "<SequenceNo>" + (CDbl(DCI("EMV_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                      "<UserTrace>Dev1</UserTrace>" +
                  "</CmdResponse>" +
              "</RStream>"
        Else
            RespXML = EMVHandler.ProcessTransaction(TXML)
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DRI("Tmp_DeviceRequestXML") = TXML
        DRI("Tmp_DeviceReady") = CheckResponse(RespXML, {"Success"})
        DRI("Tmp_DeviceResponseXML") = RespXML
        DRI("Tmp_DeviceResponseFormattedXML") = FormattedRespXml

        DRI("EMV_SequenceNo") = "0010010010"
        DRI("PDC_SequenceNo") = DCI("PDC_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("EMV_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("EMV_SequenceNo") = DRI("EMV_SequenceNo")

    End Sub

    Private Sub GetSignature()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Transaction>"

        TXML += "<HostOrIP>" + DCI("EMV_HostOrIp") + "</HostOrIP>"
        TXML += "<IpPort>" + DCI("EMV_IpPort").ToString + "</IpPort>"
        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("EMV_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("EMV_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        If Not String.IsNullOrEmpty(DCI("EMV_UserTrace")) Then
            TXML += "<UserTrace>" + DCI("EMV_UserTrace") + "</UserTrace>"
        End If

        TXML += "<TranCode>" + "GetSignature" + "</TranCode>"
        TXML += "<SecureDevice>" + DCI("EMV_SecureDevice") + "</SecureDevice>"
        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"
        TXML += "<SequenceNo>" + DCI("EMV_SequenceNo") + "</SequenceNo>"

        TXML += "</Transaction>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
              "<RStream>" +
                  "<CmdResponse>" +
                      "<ResponseOrigin>Client</ResponseOrigin>" +
                      "<DSIXReturnCode>000000</DSIXReturnCode>" +
                      "<CmdStatus>Success</CmdStatus>" +
                      "<TextResponse>Success</TextResponse>" +
                      "<SequenceNo>" + (CDbl(DCI("EMV_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                      "<UserTrace>Dev1</UserTrace>" +
                  "</CmdResponse>" +
                  "<Signature>" +
                  "12,52:13,52:12,53:12,54:11,54:11,55:10,55:11,55:11,54:12,53:12,52:13,52:14,50:14,49:15,48:17,46:18,44:19,43:20,41:21,39:22,37:24,35:25,33:26,30:28,28:29,26:30,24:31,22:32,20:33,19:34,17:34,16:35,15:35,13:36,13:37,11:37,10:37,11:37,12:37,14:37,15:37,16:37,17:36,19:36,20:36,22:36,23:36,25:36,26:36,28:37,29:37,30:37,32:38,32:38,33:39,34:40,34:41,34:43,34:44,33:45,33:47,32:48,31:50,30:51,29:53,28:54,27:56,25:57,24:59,23:60,22:61,21:62,20:62,19:63,19:64,18:65,18:65,19:65,20:65,21:65,22:65,23:64,24:64,25:64,26:64,28:64,29:63,31:63,32:63,34:63,36:63,37:63,39:63,41:63,42:63,43:63,45:63,47:63,48:63,49:63,51:64,52:64,53:64,54:64,55:#,#:103,21:104,21:103,21:103,20:104,20:104,19:104,18:104,19:103,20:103,21:103,23:102,24:102,26:102,27:101,29:101,31:100,33:100,35:99,37:99,39:98,41:98,43:97,44:97,47:97,48:96,50:96,51:96,53:96,54:96,55:96,56:95,57:96,57:95,58:96,58:#,#:100,17:100,16:101,16:102,17:103,17:104,17:105,17:106,18:106,17:107,18:108,18:109,18:110,18:111,18:112,18:113,18:114,18:115,18:116,18:#,#:92,46:93,46:94,46:94,45:95,45:96,45:97,45:98,44:99,44:100,44:101,43:102,43:103,43:104,43:105,42:107,42:108,42:109,42:111,42:112,42:113,42:114,41:115,41:116,41:117,42:118,42:#,#:99,63:99,62:99,63:100,63:101,63:102,63:103,63:104,64:105,64:106,64:107,64:108,64:109,64:110,64:111,64:113,64:114,64:#,#:166,23:167,23:167,22:168,21:168,20:168,19:168,18:169,18:168,18:168,17:169,17:169,16:169,15:169,14:169,15:168,15:168,16:167,17:167,18:167,19:166,20:166,21:166,22:165,23:165,24:164,26:164,27:163,28:163,30:163,31:163,33:162,34:162,35:162,37:162,38:162,39:162,40:162,41:162,42:163,43:163,44:163,45:164,45:164,46:165,46:166,46:167,47:168,47:169,47:170,47:172,47:173,47:174,47:176,47:177,46:179,46:180,46:182,46:184,45:186,45:187,45:189,45:190,45:192,44:194,44:195,44:196,44:198,44:200,45:201,45:202,45:203,45:205,45:#,#:" +
                  "</Signature>" +
                  "<SignMaximumX>215</SignMaximumX>" +
                  "<SignMaximumY>74</SignMaximumY>" +
              "</RStream>"
        Else
            RespXML = EMVHandler.ProcessTransaction(TXML)
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DRI("Tmp_SignatureRequestXML") = TXML
        DRI("Tmp_SignatureCaptured") = CheckResponse(RespXML, {"Success"})
        DRI("Tmp_SignatureResponseXML") = RespXML
        DRI("Tmp_SignatureResponseFormattedXML") = FormattedRespXml

        DRI("EMV_SequenceNo") = "0010010010"
        DRI("PDC_SequenceNo") = DCI("PDC_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("EMV_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("EMV_SequenceNo") = DRI("EMV_SequenceNo")

    End Sub

    Private Sub EMVBatchSummary()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Admin>"

        TXML += "<HostOrIP>" + DCI("EMV_HostOrIp") + "</HostOrIP>"
        TXML += "<IpPort>" + DCI("EMV_IpPort").ToString + "</IpPort>"
        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("EMV_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("EMV_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        If Not String.IsNullOrEmpty(DCI("EMV_UserTrace")) Then
            TXML += "<UserTrace>" + DCI("EMV_UserTrace") + "</UserTrace>"
        End If

        TXML += "<TranType>" + "Administrative" + "</TranType>"
        TXML += "<TranCode>" + "BatchSummary" + "</TranCode>"
        TXML += "<SecureDevice>" + DCI("EMV_SecureDevice") + "</SecureDevice>"
        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"
        TXML += "<SequenceNo>" + DCI("EMV_SequenceNo") + "</SequenceNo>"

        TXML += "</Admin>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Success</CmdStatus>" +
                              "<TextResponse>OK</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("EMV_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<BatchSummary>" +
                              "<MerchantID>88430043857</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<BatchNo>0099</BatchNo>" +
                              "<BatchItemCount>4</BatchItemCount>" +
                              "<NetBatchTotal>32.32</NetBatchTotal>" +
                              "<CreditPurchaseCount>2</CreditPurchaseCount>" +
                              "<CreditPurchaseAmount>22.32</CreditPurchaseAmount>" +
                              "<CreditReturnCount>1</CreditReturnCount>" +
                              "<CreditReturnAmount>10.00</CreditReturnAmount>" +
                              "<DebitPurchaseCount>3</DebitPurchaseCount>" +
                              "<DebitPurchaseAmount>25.00</DebitPurchaseAmount>" +
                              "<DebitReturnCount>1</DebitReturnCount>" +
                              "<DebitReturnAmount>5.00</DebitReturnAmount>" +
                          "</BatchSummary>" +
                      "</RStream>"
        Else
            RespXML = EMVHandler.ProcessTransaction(TXML)
        End If

        Dim FormattedRespXML = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("BatchSummaryRequestXML") = TXML
        DRI("BatchSummarySuccess") = CheckResponse(RespXML, {"Success"})
        DRI("BatchSummaryResponseXML") = RespXML
        DRI("BatchSummaryResponseFormattedXML") = FormattedRespXML

        DCI("OperationRequestXML") = TXML
        DRI("OperationSuccess") = DRI("BatchSummarySuccess")
        DRI("OperationResponseXML") = DRI("BatchSummaryResponseXML")
        DRI("OperationResponseFormattedXML") = DRI("BatchSummaryResponseFormattedXML")

        DRI("EMV_SequenceNo") = "0010010010"
        DRI("PDC_SequenceNo") = DCI("PDC_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("EMV_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("EMV_SequenceNo") = DRI("EMV_SequenceNo")

    End Sub

    Private Sub EMVBatchClose()

        EMVBatchSummary()

        If DRI("BatchSummarySuccess") Then

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<TStream>"
            TXML += "<Admin>"

            TXML += "<HostOrIP>" + DCI("EMV_HostOrIp") + "</HostOrIP>"
            TXML += "<IpPort>" + DCI("EMV_IpPort").ToString + "</IpPort>"
            TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

            If Not String.IsNullOrEmpty(DCI("EMV_TerminalID")) Then
                TXML += "<TerminalID>" + DCI("EMV_TerminalID") + "</TerminalID>"
            End If

            If Not String.IsNullOrEmpty(DCI("LaneID")) Then
                TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
            End If

            If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
                TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
            End If

            If Not String.IsNullOrEmpty(DCI("EMV_UserTrace")) Then
                TXML += "<UserTrace>" + DCI("EMV_UserTrace") + "</UserTrace>"
            End If

            TXML += "<TranType>" + "Administrative" + "</TranType>"
            TXML += "<TranCode>" + "BatchClose" + "</TranCode>"
            TXML += "<SecureDevice>" + DCI("EMV_SecureDevice") + "</SecureDevice>"
            TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"
            TXML += "<SequenceNo>" + DCI("EMV_SequenceNo") + "</SequenceNo>"

            ' ---------------------

            If GetElement("//BatchSummary/BatchNo[1]") Then

                Dim BatchNoNode = LocalData("TmpElement")

                TXML += BatchNoNode.ToString()

                For Each NextNode As XElement In BatchNoNode.ElementsAfterSelf()
                    TXML += NextNode.ToString
                Next

            End If

            ' ---------------------

            TXML += "</Admin>"
            TXML += "</TStream>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            DCI("TmpOperationRequestXML") = TXML

            Dim RespXML As String

            If DCI("TrainingMode") Then
                RespXML = "<?xml version=""1.0""?>" +
                          "<RStream>" +
                              "<CmdResponse>" +
                                  "<ResponseOrigin>Processor</ResponseOrigin>" +
                                  "<DSIXReturnCode>000000</DSIXReturnCode>" +
                                  "<CmdStatus>Success</CmdStatus>" +
                                  "<TextResponse>OK</TextResponse>" +
                                  "<SequenceNo>" + (CDbl(DCI("EMV_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                                  "<UserTrace>Dev1</UserTrace>" +
                              "</CmdResponse>" +
                              "<BatchClose>" +
                                  "<MerchantID>88430043857</MerchantID>" +
                                  "<TerminalID>002</TerminalID>" +
                                  "<OperatorID>TEST</OperatorID>" +
                                  "<BatchNo>0099</BatchNo>" +
                                  "<BatchItemCount>4</BatchItemCount>" +
                                  "<NetBatchTotal>32.32</NetBatchTotal>" +
                                  "<CreditPurchaseCount>2</CreditPurchaseCount>" +
                                  "<CreditPurchaseAmount>22.32</CreditPurchaseAmount>" +
                                  "<CreditReturnCount>1</CreditReturnCount>" +
                                  "<CreditReturnAmount>10.00</CreditReturnAmount>" +
                                  "<DebitPurchaseCount>3</DebitPurchaseCount>" +
                                  "<DebitPurchaseAmount>25.00</DebitPurchaseAmount>" +
                                  "<DebitReturnCount>1</DebitReturnCount>" +
                                  "<DebitReturnAmount>5.00</DebitReturnAmount>" +
                                  "<ControlNo>0099</ControlNo>" +
                              "</BatchClose>" +
                          "</RStream>"
            Else

                Dim BatchCount As Double, BatchAmount As Double

                If GetElementValue("//BatchSummary/BatchItemCount[1]") Then
                    BatchCount = CDbl(LocalData("TmpElementValue"))
                End If

                If GetElementValue("//BatchSummary/NetBatchTotal[1]") Then
                    BatchAmount = CDbl(LocalData("TmpElementValue"))
                End If

                If BatchCount <> 0 And BatchAmount <> 0 Then
                    RespXML = EMVHandler.ProcessTransaction(TXML)
                Else
                    RespXML = DRI("BatchSummaryResponseXML")
                End If

            End If

            Dim FormattedRespXML = GetFormattedXml(RespXML)

            If Not DCI.Exists("RespData") Then
                DRI = VB6Aux.GetMeADictionary
                DCI("RespData") = DRI
            Else
                DRI = DCI("RespData")
            End If

            DCI("BatchCloseRequestXML") = TXML
            DRI("BatchCloseSuccess") = CheckResponse(RespXML, {"Success"})
            DRI("BatchCloseResponseXML") = RespXML
            DRI("BatchCloseResponseFormattedXML") = FormattedRespXML

            DCI("OperationRequestXML") = TXML
            DRI("OperationSuccess") = DRI("BatchCloseSuccess")
            DRI("OperationResponseXML") = DRI("BatchCloseResponseXML")
            DRI("OperationResponseFormattedXML") = DRI("BatchCloseResponseFormattedXML")

            DRI("EMV_SequenceNo") = "0010010010"
            DRI("PDC_SequenceNo") = DCI("PDC_SequenceNo")

            If LocalData("ValidResponse") Then
                If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                    DRI("EMV_SequenceNo") = LocalData("TmpElementValue")
                End If
            End If

            DCI("EMV_SequenceNo") = DRI("EMV_SequenceNo")

        Else
            DRI("OperationSuccess") = DRI("BatchSummarySuccess")
            DRI("OperationResponseXML") = DRI("BatchSummaryResponseXML")
            DRI("OperationResponseFormattedXML") = DRI("BatchSummaryResponseFormattedXML")
        End If

    End Sub

    Private Sub EMVSale()

        EMVPadReset()

        If Not DRI("Tmp_DeviceReady") Then Exit Sub

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Transaction>"

        TXML += "<HostOrIP>" + DCI("EMV_HostOrIp") + "</HostOrIP>"
        TXML += "<IpPort>" + DCI("EMV_IpPort").ToString + "</IpPort>"
        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("EMV_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("EMV_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        If Not String.IsNullOrEmpty(DCI("EMV_UserTrace")) Then
            TXML += "<UserTrace>" + DCI("EMV_UserTrace") + "</UserTrace>"
        End If

        If Not String.IsNullOrEmpty(DCI("CardType")) Then
            TXML += "<CardType>" + DCI("CardType") + "</CardType>"
        End If

        TXML += "<TranCode>" + DCI("TranCode") + "</TranCode>"

        If DCI("EMV_CollectData") Then
            TXML += "<CollectData>" + "CardholderName" + "</CollectData>"
        End If

        TXML += "<SecureDevice>" + DCI("EMV_SecureDevice") + "</SecureDevice>"
        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"

        If DCI("PromptManualData") Then
            TXML += "<Account>" + "<AcctNo>" + "Prompt" + "</AcctNo>" + "</Account>"
        End If

        TXML += "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>"
        TXML += "<RefNo>" + DCI("RefNo") + "</RefNo>"

        TXML += "<Amount>"

        TXML += "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>"

        If DCI("CashbackRequested") Then
            TXML += "<CashBack>" + DCI("FormattedCashbackAmount") + "</CashBack>"
        End If

        TXML += "</Amount>"

        TXML += "<SequenceNo>" + DCI("EMV_SequenceNo") + "</SequenceNo>"

        If Not DCI("ConfirmAmount") Then
            TXML += "<OKAmount>" + "Disallow" + "</OKAmount>"
        End If

        If DCI("IgnoreDuplicateTransaction") Then
            TXML += "<Duplicate>" + "None" + "</Duplicate>"
        End If

        If DCI("PromptManualData") Then

            If Not String.IsNullOrEmpty(DCI("PromptManualData_Address")) _
            And Not String.IsNullOrEmpty(DCI("PromptManualData_Zip")) Then

                TXML += "<AVS>"

                If Not String.IsNullOrEmpty(DCI("PromptManualData_Address")) Then
                    TXML += "<Address>" + DCI("PromptManualData_Address") + "</Address>"
                End If

                If Not String.IsNullOrEmpty(DCI("PromptManualData_Zip")) Then
                    TXML += "<Zip>" + DCI("PromptManualData_Zip") + "</Zip>"
                End If

                TXML += "</AVS>"

            End If

        End If

        If DCI("AllowPartialAuth") Then
            TXML += "<PartialAuth>" + "Allow" + "</PartialAuth>"
        End If

        If DCI("RequestRecordNo") Then

            TXML += "<RecordNo>" + "RecordNumberRequested" + "</RecordNo>"

            If Not String.IsNullOrEmpty(DCI("Frequency")) Then
                TXML += "<Frequency>" + DCI("Frequency") + "</Frequency>"
            End If

        End If

        TXML += "</Transaction>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Approved</CmdStatus>" +
                              "<TextResponse>APPROVED</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("EMV_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<TranResponse>" +
                              "<MerchantID>700000000245</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<AcctNo>************0119</AcctNo>" +
                              "<CardType>VISA</CardType>" +
                              "<TranCode>EMVSale</TranCode>" +
                              "<AuthCode>036556</AuthCode>" +
                              "<AVSResult>ABC</AVSResult>" +
                              "<CVVResult>M</CVVResult>" +
                              "<CaptureStatus>Captured</CaptureStatus>" +
                              "<RefNo>" + New Random().Next(10000).ToString("00000000") + "</RefNo>" +
                              "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<Amount>" +
                                  "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>" +
                                  "<Authorize>" + DCI("FormattedPurchaseAmount") + "</Authorize>" +
                                  "<Cashback>" + IIf(DCI("CashbackRequested"), DCI("FormattedCashbackAmount"), "0.00") + "</Cashback>" +
                                  "<Gratuity>0.00</Gratuity>" +
                              "</Amount>" +
                              "<CardholderName>Test User</CardholderName>" +
                              "<AcqRefData>T305187511660041CPRL00599905||N|6.00|RN…</AcqRefData>" +
                              IIf(New Random().Next(10) = 7, "<PostProcess>EMVParamDownloadRequired</PostProcess>", "") +
                              "<ProcessData>9F37:46873A4C|9F26:D1948FBB921E62E5…</ProcessData>" +
                              IIf(DCI("RequestRecordNo"), "<RecordNo>TK:HDG4bV08i3n1He5G1AU00016</RecordNo>", "") +
                              IIf(New Random().Next(5) = 3, "<EntryMethod>MSR</EntryMethod>", "<EntryMethod>CHIP</EntryMethod>") +
                              "<Date>07/06/2015</Date>" +
                              "<Time>10:12:38</Time>" +
                              "<ApplicationLabel>Visa Credit</ApplicationLabel>" +
                              "<AID>A0000000031010</AID>" +
                              "<TVR>8000008000</TVR>" +
                              "<IAD>06010A03602000</IAD>" +
                              "<TSI>6800</TSI>" +
                              "<ARC>00</ARC>" +
                              IIf(New Random().Next(3) = 2, "<CVM>SIGN</CVM>", "<CVM>PIN VERIFIED</CVM>") +
                          "</TranResponse>" +
                          "<PrintData>" +
                              "<Line1>…</Line1>" +
                              "<LineN>…</LineN>" +
                          "</PrintData>" +
                      "</RStream>"
        Else
            RespXML = EMVHandler.ProcessTransaction(TXML)
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("OperationRequestXML") = TXML
        DRI("OperationSuccess") = CheckResponse(RespXML, {"Success", "Approved"})
        DRI("OperationResponseXML") = RespXML
        DRI("OperationResponseFormattedXML") = FormattedRespXml

        DRI("EMV_SequenceNo") = "0010010010"
        DRI("PDC_SequenceNo") = DCI("PDC_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("EMV_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("EMV_SequenceNo") = DRI("EMV_SequenceNo")

        If LocalData("ValidResponse") Then
            If CheckElement("//TranResponse/PostProcess[1]", {"EMVParamDownloadRequired"}) Then
                EMVParamDownload(True)
                DRI("EMV_PendingParamDownload") = Not DRI("PostProcess_EMVParamDownload_Completed")
            End If
        End If

        EMVPadReset()

    End Sub

    Private Sub EMVReturn()

        EMVPadReset()

        If Not DRI("Tmp_DeviceReady") Then Exit Sub

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Transaction>"

        TXML += "<HostOrIP>" + DCI("EMV_HostOrIp") + "</HostOrIP>"
        TXML += "<IpPort>" + DCI("EMV_IpPort").ToString + "</IpPort>"
        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("EMV_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("EMV_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        If Not String.IsNullOrEmpty(DCI("EMV_UserTrace")) Then
            TXML += "<UserTrace>" + DCI("EMV_UserTrace") + "</UserTrace>"
        End If

        If Not String.IsNullOrEmpty(DCI("CardType")) Then
            TXML += "<CardType>" + DCI("CardType") + "</CardType>"
        End If

        If Not String.IsNullOrEmpty(DCI("TranType")) Then
            TXML += "<TranType>" + DCI("TranType") + "</TranType>"
        End If

        TXML += "<TranCode>" + DCI("TranCode") + "</TranCode>"

        TXML += "<SecureDevice>" + DCI("EMV_SecureDevice") + "</SecureDevice>"
        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"

        If DCI("PromptManualData") Then
            TXML += "<Account>" + "<AcctNo>" + "Prompt" + "</AcctNo>" + "</Account>"
        End If

        TXML += "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>"
        TXML += "<RefNo>" + DCI("RefNo") + "</RefNo>"

        If Not String.IsNullOrEmpty(DCI("AuthCode")) Then
            TXML += "<AuthCode>" + DCI("AuthCode") + "</AuthCode>"
        End If

        TXML += "<Amount>"

        TXML += "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>"

        If DCI("CashbackRequested") Then
            TXML += "<CashBack>" + DCI("FormattedCashbackAmount") + "</CashBack>"
        End If

        TXML += "</Amount>"

        TXML += "<SequenceNo>" + DCI("EMV_SequenceNo") + "</SequenceNo>"

        If Not String.IsNullOrEmpty(DCI("AcqRefData")) Then
            TXML += "<AcqRefData>" + DCI("AcqRefData") + "</AcqRefData>"
        End If

        If Not String.IsNullOrEmpty(DCI("ProcessData")) Then
            TXML += "<ProcessData>" + DCI("ProcessData") + "</ProcessData>"
        End If

        If Not DCI("ConfirmAmount") Then
            TXML += "<OKAmount>" + "Disallow" + "</OKAmount>"
        End If

        If DCI("IgnoreDuplicateTransaction") Then
            TXML += "<Duplicate>" + "None" + "</Duplicate>"
        End If

        If DCI("AllowPartialAuth") Then
            TXML += "<PartialAuth>" + "Allow" + "</PartialAuth>"
        End If

        If DCI("RequestRecordNo") Then

            TXML += "<RecordNo>" + "RecordNumberRequested" + "</RecordNo>"

            If Not String.IsNullOrEmpty(DCI("Frequency")) Then
                TXML += "<Frequency>" + DCI("Frequency") + "</Frequency>"
            End If

        ElseIf Not String.IsNullOrEmpty(DCI("Tmp_RecordNo_Node")) Then

            TXML += "<RecordNo>" + DCI("Tmp_RecordNo_Node") + "</RecordNo>"

            If Not String.IsNullOrEmpty(DCI("Frequency")) Then
                TXML += "<Frequency>" + DCI("Frequency") + "</Frequency>"
            End If

        End If

        TXML += "</Transaction>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Approved</CmdStatus>" +
                              "<TextResponse>APPROVED</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("EMV_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<TranResponse>" +
                              "<MerchantID>700000000245</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<AcctNo>************0119</AcctNo>" +
                              "<CardType>VISA</CardType>" +
                              "<TranType>" + DCI("TranType") + "</TranType>" +
                              "<TranCode>" + DCI("TranCode") + "</TranCode>" +
                              "<AuthCode>036556</AuthCode>" +
                              "<AVSResult>ABC</AVSResult>" +
                              "<CVVResult>M</CVVResult>" +
                              "<CaptureStatus>Captured</CaptureStatus>" +
                              "<RefNo>" + New Random().Next(10000).ToString("00000000") + "</RefNo>" +
                              "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<Amount>" +
                                  "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>" +
                                  "<Authorize>" + DCI("FormattedPurchaseAmount") + "</Authorize>" +
                              "</Amount>" +
                              "<AcqRefData>T305187511660041CPRL00599905||N|6.00|RN…</AcqRefData>" +
                              "<ProcessData>9F37:46873A4C|9F26:D1948FBB921E62E5…</ProcessData>" +
                              IIf(DCI("RequestRecordNo"), "<RecordNo>TK:HDG4bV08i3n1He5G1AU00016</RecordNo>", "") +
                              IIf(New Random().Next(5) = 3, "<EntryMethod>MSR</EntryMethod>", "<EntryMethod>CHIP</EntryMethod>") +
                              "<Date>07/06/2015</Date>" +
                              "<Time>10:12:38</Time>" +
                              "<ApplicationLabel>Visa Credit</ApplicationLabel>" +
                              "<AID>A0000000031010</AID>" +
                              "<TVR>8000008000</TVR>" +
                              "<IAD>06010A03602000</IAD>" +
                              "<TSI>6800</TSI>" +
                              IIf(New Random().Next(3) = 2, "<CVM>SIGN</CVM>", "<CVM>PIN VERIFIED</CVM>") +
                          "</TranResponse>" +
                          "<PrintData>" +
                              "<Line1>…</Line1>" +
                              "<LineN>…</LineN>" +
                          "</PrintData>" +
                      "</RStream>"
        Else
            RespXML = EMVHandler.ProcessTransaction(TXML)
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("OperationRequestXML") = TXML
        DRI("OperationSuccess") = CheckResponse(RespXML, {"Success", "Approved"})
        DRI("OperationResponseXML") = RespXML
        DRI("OperationResponseFormattedXML") = FormattedRespXml

        DRI("EMV_SequenceNo") = "0010010010"
        DRI("PDC_SequenceNo") = DCI("PDC_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("EMV_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("EMV_SequenceNo") = DRI("EMV_SequenceNo")

        EMVPadReset()

    End Sub

    Private Sub InitializePDC()

        If Not LocalData.ContainsKey("PDCInitialized") Then LocalData("PDCInitialized") = False

        If Not LocalData("PDCInitialized") Then
            If String.IsNullOrEmpty(DCI("PDC_IPAddress")) Then

                PDCHandler = New DsiPDCX

                Dim RespXML = PDCHandler.ServerIPConfig(DCI("PDC_HostOrIpList"), 1)
                Dim FormattedRespXML = GetFormattedXml(RespXML)

                If Not DCI.Exists("RespData") Then
                    DRI = VB6Aux.GetMeADictionary
                    DCI("RespData") = DRI
                Else
                    DRI = DCI("RespData")
                End If

                DRI("OperationSuccess") = CheckResponse(RespXML, {"Success"})

                If DRI("OperationSuccess") And DCI("PDC_SecureDevice_RequiresInitialization") Then
                    PDC_InitSecureDevice()
                    DRI("OperationSuccess") = DRI("Tmp_PDC_DeviceReady")
                Else
                    DRI("Tmp_PDC_DeviceReady") = DRI("OperationSuccess")
                End If

                LocalData("PDCInitialized") = DRI("OperationSuccess")

            End If
        End If

    End Sub

    Private Sub PDC_InitSecureDevice()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Admin>"

        If Not String.IsNullOrEmpty(DCI("PDC_IPAddress").ToString) Then
            TXML += "<IpAddress>" + DCI("PDC_IPAddress").ToString + "</IpAddress>"
        End If

        If Not String.IsNullOrEmpty(DCI("PDC_IpPort")) Then
            TXML += "<IpPort>" + DCI("PDC_IpPort") + "</IpPort>"
        End If

        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("PDC_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("PDC_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        TXML += "<TranType>" + "Setup" + "</TranType>"
        TXML += "<TranCode>" + "SecureDeviceInit" + "</TranCode>"

        TXML += "<SecureDevice>" + DCI("PDC_SecureDevice") + "</SecureDevice>"
        TXML += "<PadType>" + DCI("PDC_SecureDevicePadType") + "</PadType>"

        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"

        TXML += "</Admin>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Approved</CmdStatus>" +
                              "<TextResponse>APPROVED</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("PDC_SequenceNo")) + 1).ToString("00000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<TranResponse>" +
                              "<MerchantID>700000000245</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<AcctNo>************0119</AcctNo>" +
                              "<ExpDate>03/24</ExpDate>" +
                              "<CardType>" + DCI("CardType") + "</CardType>" +
                              "<TranType>" + DCI("TranType") + "</TranType>" +
                              "<TranCode>" + DCI("TranCode") + "</TranCode>" +
                              "<AuthCode>036556</AuthCode>" +
                              "<AVSResult>ABC</AVSResult>" +
                              "<CVVResult>M</CVVResult>" +
                              "<CaptureStatus>Captured</CaptureStatus>" +
                              "<RefNo>" + New Random().Next(10000).ToString("00000000") + "</RefNo>" +
                              "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<Amount>" +
                                  "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>" +
                                  "<Authorize>" + DCI("FormattedPurchaseAmount") + "</Authorize>" +
                                  "<Cashback>" + IIf(DCI("CashbackRequested"), DCI("FormattedCashbackAmount"), "0.00") + "</Cashback>" +
                                  "<Balance>" + (New Random().NextDouble() * 100).ToString("0.00") + "</Balance>" +
                              "</Amount>" +
                              "<AcqRefData>T305187511660041CPRL00599905||N|6.00|RN…</AcqRefData>" +
                              "<ProcessData>9F37:46873A4C|9F26:D1948FBB921E62E5…</ProcessData>" +
                              IIf(DCI("RequestRecordNo"), "<RecordNo>TK:HDG4bV08i3n1He5G1AU00016</RecordNo>", "") +
                          "</TranResponse>" +
                      "</RStream>"
        Else
            RespXML = PDCHandler.ProcessTransaction(TXML, 1, DCI("PDC_ClientServerPassword"), DCI("PDC_UserTrace"))
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("Tmp_PDC_DeviceRequestXML") = TXML
        DRI("Tmp_PDC_DeviceReady") = CheckResponse(RespXML, {"Success"})
        DRI("Tmp_PDC_DeviceResponseXML") = RespXML
        DRI("Tmp_PDC_DeviceResponseFormattedXML") = FormattedRespXml

        DRI("PDC_SequenceNo") = "00000000"
        DRI("EMV_SequenceNo") = DCI("EMV_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("PDC_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("PDC_SequenceNo") = DRI("PDC_SequenceNo")

    End Sub

    Private Sub PDCSale()

        EMVPadReset()

        'If Not DRI("Tmp_DeviceReady") Then Exit Sub

        If DCI("TranType") = "EBT" And DCI("TranCode") = "Sale" Then

            Dim ContinueEBT As Boolean = False

            PDC_EBT_Balance()

            If DRI("EBTBalanceOperationSuccess") And LocalData("ValidResponse") Then
                If GetElementValue("//TranResponse/Amount/Balance[1]") Then

                    Dim TmpBalanceAmount As Double = Convert.ToDouble(LocalData("TmpElementValue"))

                    If TmpBalanceAmount >= DCI("TotalTransactionAmount") Then
                        If (VB6Aux.DatacapShowQuestion("Customer Balance: " + FormatNumber(TmpBalanceAmount, 2, TriState.True, TriState.False, TriState.True) + _
                        vbNewLine + "Tender Input Amount: " + FormatNumber(DCI("TotalTransactionAmount"), 2, TriState.True, TriState.False, TriState.True) + _
                        vbNewLine + vbNewLine + "¿Do you wish to proceed with the transaction?")) Then
                            ContinueEBT = True
                        End If
                    Else
                        VB6Aux.DatacapShowMessage("Customer Balance: " + FormatNumber(TmpBalanceAmount, 2, TriState.True, TriState.False, TriState.True) + _
                        vbNewLine + "Tender Input Amount: " + FormatNumber(DCI("TotalTransactionAmount"), 2, TriState.True, TriState.False, TriState.True) + _
                        vbNewLine + vbNewLine + "Not enough funds. Transaction canceled.")
                    End If

                End If
            End If

            If Not ContinueEBT Then
                DRI("NotEnoughFundsAvailable") = True
                Exit Sub
            End If

        End If

        InitializePDC()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Transaction>"

        If Not String.IsNullOrEmpty(DCI("PDC_IPAddress").ToString) Then
            TXML += "<IpAddress>" + DCI("PDC_IPAddress").ToString + "</IpAddress>"
        End If

        If Not String.IsNullOrEmpty(DCI("PDC_IpPort")) Then
            TXML += "<IpPort>" + DCI("PDC_IpPort") + "</IpPort>"
        End If

        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("PDC_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("PDC_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        TXML += "<TranType>" + DCI("TranType") + "</TranType>"

        If DCI("IgnoreDuplicateTransaction") Then
            TXML += "<Duplicate>" + "None" + "</Duplicate>"
        End If

        If Not String.IsNullOrEmpty(DCI("CardType")) Then
            TXML += "<CardType>" + DCI("CardType") + "</CardType>"
        End If

        TXML += "<TranCode>" + DCI("TranCode") + "</TranCode>"

        TXML += "<SecureDevice>" + DCI("PDC_SecureDevice") + "</SecureDevice>"

        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"

        TXML += "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>"
        TXML += "<RefNo>" + DCI("RefNo") + "</RefNo>"

        If DCI("PromptManualData") Then
            TXML += "<Account>" + "<AcctNo>" + "Prompt" + "</AcctNo>" + "</Account>"
        Else
            TXML += "<Account>" + "<AcctNo>" + "SecureDevice" + "</AcctNo>" + "</Account>"
        End If

        TXML += "<Amount>"

        TXML += "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>"

        If DCI("CashbackRequested") Then
            TXML += "<CashBack>" + DCI("FormattedCashbackAmount") + "</CashBack>"
        End If

        TXML += "</Amount>"

        TXML += "<SequenceNo>" + DCI("PDC_SequenceNo") + "</SequenceNo>"

        If DCI("PromptManualData") Then

            If Not String.IsNullOrEmpty(DCI("TranInfo_AuthCode")) _
            And Not String.IsNullOrEmpty(DCI("TranInfo_VoucherNo")) Then

                TXML += "<TranInfo>"

                If Not String.IsNullOrEmpty(DCI("TranInfo_AuthCode")) Then
                    TXML += "<AuthCode>" + DCI("TranInfo_AuthCode") + "</AuthCode>"
                End If

                If Not String.IsNullOrEmpty(DCI("TranInfo_VoucherNo")) Then
                    TXML += "<VoucherNo>" + DCI("TranInfo_VoucherNo") + "</VoucherNo>"
                End If

                TXML += "</TranInfo>"

            End If

        End If

        TXML += "</Transaction>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Approved</CmdStatus>" +
                              "<TextResponse>APPROVED</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("PDC_SequenceNo")) + 1).ToString("00000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<TranResponse>" +
                              "<MerchantID>700000000245</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<AcctNo>************0119</AcctNo>" +
                              "<ExpDate>03/24</ExpDate>" +
                              "<CardType>" + DCI("CardType") + "</CardType>" +
                              "<TranType>" + DCI("TranType") + "</TranType>" +
                              "<TranCode>" + DCI("TranCode") + "</TranCode>" +
                              "<AuthCode>036556</AuthCode>" +
                              "<AVSResult>ABC</AVSResult>" +
                              "<CVVResult>M</CVVResult>" +
                              "<CaptureStatus>Captured</CaptureStatus>" +
                              "<RefNo>" + New Random().Next(10000).ToString("00000000") + "</RefNo>" +
                              "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<Amount>" +
                                  "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>" +
                                  "<Authorize>" + DCI("FormattedPurchaseAmount") + "</Authorize>" +
                                  "<Cashback>" + IIf(DCI("CashbackRequested"), DCI("FormattedCashbackAmount"), "0.00") + "</Cashback>" +
                                  "<Balance>" + (New Random().NextDouble() * 100).ToString("0.00") + "</Balance>" +
                              "</Amount>" +
                              "<AcqRefData>T305187511660041CPRL00599905||N|6.00|RN…</AcqRefData>" +
                              IIf(New Random().Next(10) = 7, "<PostProcess>EMVParamDownloadRequired</PostProcess>", "") +
                              "<ProcessData>9F37:46873A4C|9F26:D1948FBB921E62E5…</ProcessData>" +
                              IIf(DCI("RequestRecordNo"), "<RecordNo>TK:HDG4bV08i3n1He5G1AU00016</RecordNo>", "") +
                          "</TranResponse>" +
                      "</RStream>"
        Else
            RespXML = PDCHandler.ProcessTransaction(TXML, 1, DCI("PDC_ClientServerPassword"), DCI("PDC_UserTrace"))
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("OperationRequestXML") = TXML
        DRI("OperationSuccess") = CheckResponse(RespXML, {"Success", "Approved"})
        DRI("OperationResponseXML") = RespXML
        DRI("OperationResponseFormattedXML") = FormattedRespXml

        DRI("PDC_SequenceNo") = "00000000"
        DRI("EMV_SequenceNo") = DCI("EMV_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("PDC_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("PDC_SequenceNo") = DRI("PDC_SequenceNo")

    End Sub

    Private Sub PDC_EBT_Balance()

        InitializePDC()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Transaction>"

        If Not String.IsNullOrEmpty(DCI("PDC_IPAddress").ToString) Then
            TXML += "<IpAddress>" + DCI("PDC_IPAddress").ToString + "</IpAddress>"
        End If

        If Not String.IsNullOrEmpty(DCI("PDC_IpPort")) Then
            TXML += "<IpPort>" + DCI("PDC_IpPort") + "</IpPort>"
        End If

        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("PDC_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("PDC_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        TXML += "<TranType>" + DCI("TranType") + "</TranType>"

        If DCI("IgnoreDuplicateTransaction") Then
            TXML += "<Duplicate>" + "None" + "</Duplicate>"
        End If

        If Not String.IsNullOrEmpty(DCI("CardType")) Then
            TXML += "<CardType>" + DCI("CardType") + "</CardType>"
        End If

        TXML += "<TranCode>" + "Balance" + "</TranCode>"

        TXML += "<SecureDevice>" + DCI("PDC_SecureDevice") + "</SecureDevice>"

        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"

        TXML += "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>"
        TXML += "<RefNo>" + DCI("RefNo") + "</RefNo>"

        If DCI("PromptManualData") Then
            TXML += "<Account>" + "<AcctNo>" + "Prompt" + "</AcctNo>" + "</Account>"
        Else
            TXML += "<Account>" + "<AcctNo>" + "SecureDevice" + "</AcctNo>" + "</Account>"
        End If

        TXML += "<Amount>"

        TXML += "<Purchase>" + "0.00" + "</Purchase>"

        TXML += "</Amount>"

        TXML += "<SequenceNo>" + DCI("PDC_SequenceNo") + "</SequenceNo>"

        If DCI("PromptManualData") Then

            If Not String.IsNullOrEmpty(DCI("TranInfo_AuthCode")) _
            And Not String.IsNullOrEmpty(DCI("TranInfo_VoucherNo")) Then

                TXML += "<TranInfo>"

                If Not String.IsNullOrEmpty(DCI("TranInfo_AuthCode")) Then
                    TXML += "<AuthCode>" + DCI("TranInfo_AuthCode") + "</AuthCode>"
                End If

                If Not String.IsNullOrEmpty(DCI("TranInfo_VoucherNo")) Then
                    TXML += "<VoucherNo>" + DCI("TranInfo_VoucherNo") + "</VoucherNo>"
                End If

                TXML += "</TranInfo>"

            End If

        End If

        TXML += "</Transaction>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Approved</CmdStatus>" +
                              "<TextResponse>APPROVED</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("PDC_SequenceNo")) + 1).ToString("00000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<TranResponse>" +
                              "<MerchantID>700000000245</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<AcctNo>************0119</AcctNo>" +
                              "<ExpDate>03/24</ExpDate>" +
                              "<CardType>" + DCI("CardType") + "</CardType>" +
                              "<TranType>" + DCI("TranType") + "</TranType>" +
                              "<TranCode>" + DCI("TranCode") + "</TranCode>" +
                              "<AuthCode>036556</AuthCode>" +
                              "<AVSResult>ABC</AVSResult>" +
                              "<CVVResult>M</CVVResult>" +
                              "<CaptureStatus>Captured</CaptureStatus>" +
                              "<RefNo>" + New Random().Next(10000).ToString("00000000") + "</RefNo>" +
                              "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<Amount>" +
                                  "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>" +
                                  "<Authorize>" + DCI("FormattedPurchaseAmount") + "</Authorize>" +
                                  "<Cashback>" + IIf(DCI("CashbackRequested"), DCI("FormattedCashbackAmount"), "0.00") + "</Cashback>" +
                                  "<Balance>" + (New Random().NextDouble() * 100).ToString("0.00") + "</Balance>" +
                              "</Amount>" +
                              "<AcqRefData>T305187511660041CPRL00599905||N|6.00|RN…</AcqRefData>" +
                              "<ProcessData>9F37:46873A4C|9F26:D1948FBB921E62E5…</ProcessData>" +
                              IIf(DCI("RequestRecordNo"), "<RecordNo>TK:HDG4bV08i3n1He5G1AU00016</RecordNo>", "") +
                          "</TranResponse>" +
                      "</RStream>"
        Else
            RespXML = PDCHandler.ProcessTransaction(TXML, 1, DCI("PDC_ClientServerPassword"), DCI("PDC_UserTrace"))
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("EBTBalanceOperationRequestXML") = TXML
        DRI("EBTBalanceOperationSuccess") = CheckResponse(RespXML, {"Success", "Approved"})
        DRI("EBTBalanceOperationResponseXML") = RespXML
        DRI("EBTBalanceOperationResponseFormattedXML") = FormattedRespXml

        DRI("PDC_SequenceNo") = "00000000"
        DRI("EMV_SequenceNo") = DCI("EMV_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("PDC_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("PDC_SequenceNo") = DRI("PDC_SequenceNo")

    End Sub

    Private Sub PDCReturn()

        EMVPadReset()

        'If Not DRI("Tmp_DeviceReady") Then Exit Sub

        InitializePDC()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Transaction>"

        If Not String.IsNullOrEmpty(DCI("PDC_IPAddress").ToString) Then
            TXML += "<IpAddress>" + DCI("PDC_IPAddress").ToString + "</IpAddress>"
        End If

        If Not String.IsNullOrEmpty(DCI("PDC_IpPort")) Then
            TXML += "<IpPort>" + DCI("PDC_IpPort") + "</IpPort>"
        End If

        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("PDC_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("PDC_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        TXML += "<TranType>" + DCI("TranType") + "</TranType>"

        If DCI("IgnoreDuplicateTransaction") Then
            TXML += "<Duplicate>" + "None" + "</Duplicate>"
        End If

        If Not String.IsNullOrEmpty(DCI("CardType")) Then
            TXML += "<CardType>" + DCI("CardType") + "</CardType>"
        End If

        TXML += "<TranCode>" + DCI("TranCode") + "</TranCode>"

        TXML += "<SecureDevice>" + DCI("PDC_SecureDevice") + "</SecureDevice>"

        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"

        TXML += "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>"
        TXML += "<RefNo>" + DCI("RefNo") + "</RefNo>"

        If DCI("PromptManualData") Then
            TXML += "<Account>" + "<AcctNo>" + "Prompt" + "</AcctNo>" + "</Account>"
        Else
            TXML += "<Account>" + "<AcctNo>" + "SecureDevice" + "</AcctNo>" + "</Account>"
        End If

        TXML += "<Amount>"

        TXML += "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>"

        If DCI("CashbackRequested") Then
            TXML += "<CashBack>" + DCI("FormattedCashbackAmount") + "</CashBack>"
        End If

        TXML += "</Amount>"

        TXML += "<SequenceNo>" + DCI("PDC_SequenceNo") + "</SequenceNo>"

        If DCI("PromptManualData") Then

            If Not String.IsNullOrEmpty(DCI("TranInfo_AuthCode")) _
            And Not String.IsNullOrEmpty(DCI("TranInfo_VoucherNo")) Then

                TXML += "<TranInfo>"

                If Not String.IsNullOrEmpty(DCI("TranInfo_AuthCode")) Then
                    TXML += "<AuthCode>" + DCI("TranInfo_AuthCode") + "</AuthCode>"
                End If

                If Not String.IsNullOrEmpty(DCI("TranInfo_VoucherNo")) Then
                    TXML += "<VoucherNo>" + DCI("TranInfo_VoucherNo") + "</VoucherNo>"
                End If

                TXML += "</TranInfo>"

            End If

        End If

        TXML += "</Transaction>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Approved</CmdStatus>" +
                              "<TextResponse>APPROVED</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("PDC_SequenceNo")) + 1).ToString("00000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<TranResponse>" +
                              "<MerchantID>700000000245</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<AcctNo>************0119</AcctNo>" +
                              "<ExpDate>03/24</ExpDate>" +
                              "<CardType>" + DCI("CardType") + "</CardType>" +
                              "<TranType>" + DCI("TranType") + "</TranType>" +
                              "<TranCode>" + DCI("TranCode") + "</TranCode>" +
                              "<AuthCode>036556</AuthCode>" +
                              "<AVSResult>ABC</AVSResult>" +
                              "<CVVResult>M</CVVResult>" +
                              "<CaptureStatus>Captured</CaptureStatus>" +
                              "<RefNo>" + New Random().Next(10000).ToString("00000000") + "</RefNo>" +
                              "<InvoiceNo>" + DCI("InvoiceNo") + "</InvoiceNo>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<Amount>" +
                                  "<Purchase>" + DCI("FormattedPurchaseAmount") + "</Purchase>" +
                                  "<Authorize>" + DCI("FormattedPurchaseAmount") + "</Authorize>" +
                                  "<Cashback>" + IIf(DCI("CashbackRequested"), DCI("FormattedCashbackAmount"), "0.00") + "</Cashback>" +
                                  "<Balance>" + (New Random().NextDouble() * 100).ToString("0.00") + "</Balance>" +
                              "</Amount>" +
                              "<AcqRefData>T305187511660041CPRL00599905||N|6.00|RN…</AcqRefData>" +
                              IIf(New Random().Next(10) = 7, "<PostProcess>EMVParamDownloadRequired</PostProcess>", "") +
                              "<ProcessData>9F37:46873A4C|9F26:D1948FBB921E62E5…</ProcessData>" +
                              IIf(DCI("RequestRecordNo"), "<RecordNo>TK:HDG4bV08i3n1He5G1AU00016</RecordNo>", "") +
                          "</TranResponse>" +
                      "</RStream>"
        Else
            RespXML = PDCHandler.ProcessTransaction(TXML, 1, DCI("PDC_ClientServerPassword"), DCI("PDC_UserTrace"))
        End If

        Dim FormattedRespXml = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("OperationRequestXML") = TXML
        DRI("OperationSuccess") = CheckResponse(RespXML, {"Success", "Approved"})
        DRI("OperationResponseXML") = RespXML
        DRI("OperationResponseFormattedXML") = FormattedRespXml

        DRI("PDC_SequenceNo") = "00000000"
        DRI("EMV_SequenceNo") = DCI("EMV_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("PDC_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("PDC_SequenceNo") = DRI("PDC_SequenceNo")

    End Sub

    Private Sub PDCBatchSummary()

        Dim TmpXml1 As XmlDocument = New XmlDocument()

        Dim TXML As String = String.Empty

        TXML += "<TStream>"
        TXML += "<Admin>"

        If Not String.IsNullOrEmpty(DCI("PDC_IPAddress").ToString) Then
            TXML += "<IpAddress>" + DCI("PDC_IPAddress").ToString + "</IpAddress>"
        End If

        If Not String.IsNullOrEmpty(DCI("PDC_IpPort")) Then
            TXML += "<IpPort>" + DCI("PDC_IpPort") + "</IpPort>"
        End If

        TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

        If Not String.IsNullOrEmpty(DCI("PDC_TerminalID")) Then
            TXML += "<TerminalID>" + DCI("PDC_TerminalID") + "</TerminalID>"
        End If

        If Not String.IsNullOrEmpty(DCI("LaneID")) Then
            TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
        End If

        If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
            TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
        End If

        TXML += "<TranCode>" + "BatchSummary" + "</TranCode>"
        TXML += "<SecureDevice>" + DCI("PDC_SecureDevice") + "</SecureDevice>"
        TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"
        TXML += "<SequenceNo>" + DCI("PDC_SequenceNo") + "</SequenceNo>"

        TXML += "</Admin>"
        TXML += "</TStream>"

        FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

        DCI("TmpOperationRequestXML") = TXML

        Dim RespXML As String

        If DCI("TrainingMode") Then
            RespXML = "<?xml version=""1.0""?>" +
                      "<RStream>" +
                          "<CmdResponse>" +
                              "<ResponseOrigin>Processor</ResponseOrigin>" +
                              "<DSIXReturnCode>000000</DSIXReturnCode>" +
                              "<CmdStatus>Success</CmdStatus>" +
                              "<TextResponse>OK</TextResponse>" +
                              "<SequenceNo>" + (CDbl(DCI("PDC_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                              "<UserTrace>Dev1</UserTrace>" +
                          "</CmdResponse>" +
                          "<BatchSummary>" +
                              "<MerchantID>88430043857</MerchantID>" +
                              "<TerminalID>002</TerminalID>" +
                              "<OperatorID>TEST</OperatorID>" +
                              "<BatchNo>0099</BatchNo>" +
                              "<BatchItemCount>4</BatchItemCount>" +
                              "<NetBatchTotal>32.32</NetBatchTotal>" +
                              "<CreditPurchaseCount>2</CreditPurchaseCount>" +
                              "<CreditPurchaseAmount>22.32</CreditPurchaseAmount>" +
                              "<CreditReturnCount>1</CreditReturnCount>" +
                              "<CreditReturnAmount>10.00</CreditReturnAmount>" +
                              "<DebitPurchaseCount>3</DebitPurchaseCount>" +
                              "<DebitPurchaseAmount>25.00</DebitPurchaseAmount>" +
                              "<DebitReturnCount>1</DebitReturnCount>" +
                              "<DebitReturnAmount>5.00</DebitReturnAmount>" +
                          "</BatchSummary>" +
                      "</RStream>"
        Else
            RespXML = PDCHandler.ProcessTransaction(TXML, 1, DCI("PDC_ClientServerPassword"), DCI("PDC_UserTrace"))
        End If

        Dim FormattedRespXML = GetFormattedXml(RespXML)

        If Not DCI.Exists("RespData") Then
            DRI = VB6Aux.GetMeADictionary
            DCI("RespData") = DRI
        Else
            DRI = DCI("RespData")
        End If

        DCI("BatchSummaryRequestXML") = TXML
        DRI("BatchSummarySuccess") = CheckResponse(RespXML, {"Success"})
        DRI("BatchSummaryResponseXML") = RespXML
        DRI("BatchSummaryResponseFormattedXML") = FormattedRespXML

        DCI("OperationRequestXML") = TXML
        DRI("OperationSuccess") = DRI("BatchSummarySuccess")
        DRI("OperationResponseXML") = DRI("BatchSummaryResponseXML")
        DRI("OperationResponseFormattedXML") = DRI("BatchSummaryResponseFormattedXML")

        DRI("PDC_SequenceNo") = "00000000"
        DRI("EMV_SequenceNo") = DCI("EMV_SequenceNo")

        If LocalData("ValidResponse") Then
            If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                DRI("PDC_SequenceNo") = LocalData("TmpElementValue")
            End If
        End If

        DCI("PDC_SequenceNo") = DRI("PDC_SequenceNo")

    End Sub

    Private Sub PDCBatchClose()

        PDCBatchSummary()

        If DRI("BatchSummarySuccess") Then

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<TStream>"
            TXML += "<Admin>"

            If Not String.IsNullOrEmpty(DCI("PDC_IPAddress").ToString) Then
                TXML += "<IpAddress>" + DCI("PDC_IPAddress").ToString + "</IpAddress>"
            End If

            If Not String.IsNullOrEmpty(DCI("PDC_IpPort")) Then
                TXML += "<IpPort>" + DCI("PDC_IpPort") + "</IpPort>"
            End If

            TXML += "<MerchantID>" + DCI("MerchantID") + "</MerchantID>"

            If Not String.IsNullOrEmpty(DCI("PDC_TerminalID")) Then
                TXML += "<TerminalID>" + DCI("PDC_TerminalID") + "</TerminalID>"
            End If

            If Not String.IsNullOrEmpty(DCI("LaneID")) Then
                TXML += "<LaneID>" + DCI("LaneID") + "</LaneID>"
            End If

            If Not String.IsNullOrEmpty(DCI("OperatorID")) Then
                TXML += "<OperatorID>" + DCI("OperatorID") + "</OperatorID>"
            End If

            TXML += "<TranCode>" + "BatchClose" + "</TranCode>"
            TXML += "<SecureDevice>" + DCI("PDC_SecureDevice") + "</SecureDevice>"
            TXML += "<ComPort>" + DCI("COMPort").ToString + "</ComPort>"
            TXML += "<SequenceNo>" + DCI("PDC_SequenceNo") + "</SequenceNo>"

            ' ---------------------

            If GetElement("//BatchSummary/BatchNo[1]") Then

                Dim BatchNoNode = LocalData("TmpElement")

                TXML += BatchNoNode.ToString()

                For Each NextNode As XElement In BatchNoNode.ElementsAfterSelf()
                    TXML += NextNode.ToString
                Next

            End If

            ' ---------------------

            TXML += "</Admin>"
            TXML += "</TStream>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            DCI("TmpOperationRequestXML") = TXML

            Dim RespXML As String

            If DCI("TrainingMode") Then
                RespXML = "<?xml version=""1.0""?>" +
                          "<RStream>" +
                              "<CmdResponse>" +
                                  "<ResponseOrigin>Processor</ResponseOrigin>" +
                                  "<DSIXReturnCode>000000</DSIXReturnCode>" +
                                  "<CmdStatus>Success</CmdStatus>" +
                                  "<TextResponse>OK</TextResponse>" +
                                  "<SequenceNo>" + (CDbl(DCI("PDC_SequenceNo")) + 1).ToString("0000000000") + "</SequenceNo>" +
                                  "<UserTrace>Dev1</UserTrace>" +
                              "</CmdResponse>" +
                              "<BatchSummary>" +
                                  "<MerchantID>88430043857</MerchantID>" +
                                  "<TerminalID>002</TerminalID>" +
                                  "<OperatorID>TEST</OperatorID>" +
                                  "<BatchNo>0099</BatchNo>" +
                                  "<BatchItemCount>4</BatchItemCount>" +
                                  "<NetBatchTotal>32.32</NetBatchTotal>" +
                                  "<CreditPurchaseCount>2</CreditPurchaseCount>" +
                                  "<CreditPurchaseAmount>22.32</CreditPurchaseAmount>" +
                                  "<CreditReturnCount>1</CreditReturnCount>" +
                                  "<CreditReturnAmount>10.00</CreditReturnAmount>" +
                                  "<DebitPurchaseCount>3</DebitPurchaseCount>" +
                                  "<DebitPurchaseAmount>25.00</DebitPurchaseAmount>" +
                                  "<DebitReturnCount>1</DebitReturnCount>" +
                                  "<DebitReturnAmount>5.00</DebitReturnAmount>" +
                              "</BatchSummary>" +
                          "</RStream>"
            Else

                Dim BatchCount As Double, BatchAmount As Double

                If GetElementValue("//BatchSummary/BatchItemCount[1]") Then
                    BatchCount = CDbl(LocalData("TmpElementValue"))
                End If

                If GetElementValue("//BatchSummary/NetBatchTotal[1]") Then
                    BatchAmount = CDbl(LocalData("TmpElementValue"))
                End If

                If BatchCount <> 0 And BatchAmount <> 0 Then
                    RespXML = PDCHandler.ProcessTransaction(TXML, 1, DCI("PDC_ClientServerPassword"), DCI("PDC_UserTrace"))
                Else
                    RespXML = DRI("BatchSummaryResponseXML")
                End If

            End If

            Dim FormattedRespXML = GetFormattedXml(RespXML)

            If Not DCI.Exists("RespData") Then
                DRI = VB6Aux.GetMeADictionary
                DCI("RespData") = DRI
            Else
                DRI = DCI("RespData")
            End If

            DCI("BatchCloseRequestXML") = TXML
            DRI("BatchCloseSuccess") = CheckResponse(RespXML, {"Success"})
            DRI("BatchCloseResponseXML") = RespXML
            DRI("BatchCloseResponseFormattedXML") = FormattedRespXML

            DCI("OperationRequestXML") = TXML
            DRI("OperationSuccess") = DRI("BatchCloseSuccess")
            DRI("OperationResponseXML") = DRI("BatchCloseResponseXML")
            DRI("OperationResponseFormattedXML") = DRI("BatchCloseResponseFormattedXML")

            DRI("PDC_SequenceNo") = "00000000"
            DRI("EMV_SequenceNo") = DCI("EMV_SequenceNo")

            If LocalData("ValidResponse") Then
                If GetElementValue("//CmdResponse/SequenceNo[1]") Then
                    DRI("PDC_SequenceNo") = LocalData("TmpElementValue")
                End If
            End If

            DCI("PDC_SequenceNo") = DRI("PDC_SequenceNo")

        Else
            DCI("OperationRequestXML") = DRI("BatchSummaryRequestXML")
            DRI("OperationSuccess") = DRI("BatchSummarySuccess")
            DRI("OperationResponseXML") = DRI("BatchSummaryResponseXML")
            DRI("OperationResponseFormattedXML") = DRI("BatchSummaryResponseFormattedXML")
        End If

    End Sub

    Private Function GetFormattedXml(ByVal XmlStr As String, Optional ByVal DeclarationText As String = "") As String
        Return FormatXml(XmlStr, DeclarationText)
    End Function

    Private Function FormatXml(ByVal XmlStr As String, Optional ByVal DeclarationText As String = "") As String

        Try

            Dim TmpDoc As XDocument = XDocument.Parse(XmlStr)

            XmlStr = TmpDoc.ToString()

            If Not String.IsNullOrEmpty(DeclarationText) Then
                XmlStr = DeclarationText + vbNewLine + XmlStr
            End If

            Return XmlStr

        Catch ex As Exception
            Return XmlStr
        End Try

    End Function

End Class
