﻿Public Class Form1

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Console.WriteLine(Handler.ResponseInfo("SerialPorts"))
        'Console.WriteLine(Handler.ResponseInfo("ChipDevicesInfo").ToString)
        'Console.WriteLine(Handler.ResponseInfo("SwipeDevicesInfo").ToString)

        Dim Dt As Scripting.Dictionary
        Dt = New Scripting.Dictionary

        Dt("TrainingMode") = True

        Dt("SetupFilePath") = "D:\Ultimos Fuentes\TmpProjects\MERCHANT USA RESOURCES\isPOS\Setup.ini"
        Dt("MainOperationType") = "Sale" 'BC
        'Dt("MainOperationType") = "BatchClose"
        Dt("POSCashierCode") = "312181230"
        Dt("POSCashierName") = "BIGWISE"
        Dt("POSNumber") = "002"
        Dt("POSInvoice") = "002000141" 'BC
        Dt("PurchaseAmount") = 75 'BC
        Dt("TenderInfoObj") = Nothing
        Dt("CurrencyInfoObj") = Nothing
        Dt("RelatedTransXMLResponse") = ""
        Dt("StellarProductCode") = 853
        Dt("CallerAppName") = "Stellar POS"
        Dt("EMV_HostOrIp") = "testhost5.dsipscs.com"
        Dt("PDC_IPAddress") = "98.140.102.201" 'Dt("EMV_HostOrIp")
        Dt("EMV_IpPort") = 9000
        Dt("EMV_PendingParamDownload") = 1
        'Dt("PDC_HostOrIpList") = "testhost5.dsipscs.com;192.168.1.1;79.34.56.123"
        Dt("PDC_ConnectTimeout") = 7000
        Dt("PDC_ResponseTimeout") = 300
        Dt("MerchantID") = "101438" '"700000000245"
        Dt("EMV_TerminalID") = "00000001"
        Dt("PDC_TerminalID") = "00000001"
        Dt("OperatorID") = "TEST"
        Dt("EMV_UserTrace") = ""
        Dt("EMV_CollectData") = True
        Dt("EMV_SecureDevice") = "NONE"
        Dt("PDC_SecureDevice") = "NONE"
        Dt("PDC_SecureDevicePadType") = "NONE"
        Dt("PDC_SecureDevice_RequiresInitialization") = False
        Dt("COMPort") = 1
        Dt("Printing_Source") = 0
        Dt("Printing_PrinterDeviceName") = ""
        Dt("Printing_PrintDeclines") = 0
        Dt("Printing_MaxLineChars") = 38
        Dt("Printing_Header") = "[CENTERLN]MERCHANT NAME[VBNEWLINE][CENTERLN]MERCHANT STREET[VBNEWLINE][CENTERLN]CITY, STATE ZIP[VBNEWLINE][CENTERLN]123-456-7890"
        Dt("Printing_Footer") = ""
        Dt("InvoiceNo") = "002000141" 'BC
        Dt("RefNo") = "002000141" 'BC
        Dt("FormattedPurchaseAmount") = "75.00" 'BC
        Dt("CashbackRequested") = False 'BC
        Dt("CashbackAmount") = 0 'BC
        Dt("TotalTransactionAmount") = 75 'BC
        Dt("EMV_SequenceNo") = "0010010010"
        Dt("PDC_SequenceNo") = "00000000"
        Dt("ConfirmAmount") = True
        Dt("IgnoreDuplicateTransaction") = False
        Dt("AllowPartialAuth") = False
        Dt("RequestRecordNo") = False
        Dt("Frequency") = ""
        Dt("TranType") = "EBT" 'BC
        'Dt("TranType") = "Administrative"
        'Dt("TranCode") = "Voucher"
        Dt("TranCode") = "Sale" 'BC
        'Dt("TranCode") = "BatchClose"
        Dt("CardType") = "Foodstamp" 'BC
        Dt("TranInfo_AuthCode") = "Prompt" 'BC
        Dt("TranInfo_VoucherNo") = "Prompt" 'BC
        Dt("AuthCode") = "" 'BC
        Dt("AcqRefData") = "" 'BC
        Dt("ProcessData") = "" 'BC
        Dt("PromptManualData") = True 'BC
        Dt("PromptManualData_Address") = "" 'BC
        Dt("PromptManualData_Zip") = "" 'BC
        Dt("EMVEnabled") = False 'BC

        'Dt("POSCashierName") = "BIGWISE"
        'Dt("POSCashierName") = "BIGWISE"
        'Dt("POSCashierName") = "BIGWISE"
        'Dt("POSCashierName") = "BIGWISE"
        'Dt("POSCashierName") = "BIGWISE"

        Dim Handler As BIGWISE_USDatacapHandler.Main

        Handler = New BIGWISE_USDatacapHandler.Main

        Handler.RequestInfo = Dt

        Handler.ResponseInfo = Nothing

        Handler.VB6AuxObj = Me

        Handler.BeginProcess()

    End Sub

    Public Sub DatacapShowMessage(ByVal pMsg As String)
        MsgBox(pMsg)
    End Sub

    Public Function DatacapShowQuestion(ByVal pMsg As String) As Boolean
        DatacapShowQuestion = (MsgBox(pMsg, MsgBoxStyle.OkCancel) = MsgBoxResult.Ok)
    End Function

    Public Function GetMeADictionary() As Object
        GetMeADictionary = New Scripting.Dictionary
    End Function

End Class
